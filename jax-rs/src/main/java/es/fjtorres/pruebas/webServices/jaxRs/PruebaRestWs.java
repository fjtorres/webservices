package es.fjtorres.pruebas.webServices.jaxRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.fjtorres.pruebas.webServices.core.Coche;

@Path("/prueba")
public class PruebaRestWs {

	
    @GET
    @Path("hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String helloWord() {
        return "Hello word";
    }
    

    @GET
    @Path("coche")
    @Produces(MediaType.APPLICATION_JSON)
    public Coche getCoche() {
		Coche coche = new Coche();;
		coche.setAnio(2014);
		coche.setCocheId(1L);
		coche.setMarca("FORD");
		coche.setModelo("FOCUS");
		return coche;
	}

    @PUT
    @Path("coche")
    @Consumes(MediaType.APPLICATION_JSON)
	public void load(Coche coche){
		System.out.println(coche.getAnio());
	}
}
