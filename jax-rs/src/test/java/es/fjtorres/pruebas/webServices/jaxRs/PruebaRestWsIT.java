package es.fjtorres.pruebas.webServices.jaxRs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import es.fjtorres.pruebas.webServices.core.Coche;

public class PruebaRestWsIT extends JerseyTest {

	@Override
	protected Application configure() {
		return new ResourceConfig(PruebaRestWs.class);
	}

	@Test
	public void testHelloWord() {
		final String response = target("/prueba/hello").request().get(
				String.class);
		assertEquals("Hello word", response);
	}

	@Test
	public void testGetCoche() {
		final Coche response = target("/prueba/coche").request()
				.accept(MediaType.APPLICATION_JSON).get(Coche.class);
		assertNotNull(response);
	}

	@Test
	public void testLoad() {
		Response response = target("/prueba/coche").request().put(
				Entity.json(new Coche()));
		System.out.println(response.getStatus());
	}

}
