package es.fjtorres.pruebas.webServices.jaxRs;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import es.fjtorres.pruebas.webServices.core.Coche;

public class PruebaRestWsTest {

	private PruebaRestWs instance;
	
	@Before
	public void setUp () {
		instance = new PruebaRestWs();
	}
	
	@Test
	public void testHelloWord() {
		assertEquals("Hello word", instance.helloWord());
	}

	@Test
	public void testGetCoche() {
		assertNotNull(instance.getCoche());
	}

	@Test
	public void testLoad() {
		instance.load(new Coche());
	}

}
