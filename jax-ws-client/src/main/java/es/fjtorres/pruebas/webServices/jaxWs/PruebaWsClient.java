package es.fjtorres.pruebas.webServices.jaxWs;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class PruebaWsClient {

	private static final String WS_NAMESPACE = "http://jaxWs.webServices.pruebas.fjtorres.es/";
	private static final String WS_ENDPOINT = "http://localhost:8080/jax-ws/webapi/prueba";
	
	public void testHelloWord() throws MalformedURLException {

		// Data to access the web service
		final URL wsdlDocumentLocation = new URL(WS_ENDPOINT + "?wsdl");
		final String servicePart = "PruebaWsService";
		final String portName = "PruebaWsPort";
		final QName serviceQN = new QName(WS_NAMESPACE, servicePart);
		final QName portQN = new QName(WS_NAMESPACE, portName);

		// Creates a service instance
		final Service service = Service.create(wsdlDocumentLocation, serviceQN);
		final IPruebaWs ws = service.getPort(portQN, IPruebaWs.class);
		
		System.out.println(ws.getCoche());
	}
	
	public static void main(String[] args) throws Exception {
		new PruebaWsClient().testHelloWord();
	}
}
