package es.fjtorres.pruebas.webServices.jaxWs;

import javax.jws.WebService;

@WebService
public interface IPruebaWs {

	String helloWord();
	Coche getCoche();
	void load(Coche coche);

}