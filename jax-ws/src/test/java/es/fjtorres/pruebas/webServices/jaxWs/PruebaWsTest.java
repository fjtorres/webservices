package es.fjtorres.pruebas.webServices.jaxWs;

import static org.junit.Assert.*;

import org.junit.Test;

public class PruebaWsTest {

	@Test
	public void testHelloWord() {
		final PruebaWs ws = new PruebaWs();
		assertEquals("Hello word", ws.helloWord());
	}

}
