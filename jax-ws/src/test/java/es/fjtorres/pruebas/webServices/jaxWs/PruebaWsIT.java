package es.fjtorres.pruebas.webServices.jaxWs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import org.junit.Test;

/**
 * Test de integración para el servicio web {@link PruebaWs}.
 * <p>
 * Siguiendo como ejemplo:
 * http://antoniogoncalves.org/2012/10/24/no-you-dont-need-to-mock-your-
 * soap-web-service-to-test-it/
 * 
 * @author fjtorres
 * 
 */
public class PruebaWsIT {

	private static final String SOAP_BINDING = "http://schemas.xmlsoap.org/wsdl/soap/http";
	private static final String WS_NAMESPACE = "http://jaxWs.webServices.pruebas.fjtorres.es/";
	private static final String WS_ENDPOINT = "http://localhost:8080/webapi/prueba";

	@Test
	public void testHelloWord() throws MalformedURLException {
		final Endpoint endpoint = Endpoint.publish(WS_ENDPOINT, new PruebaWs());

		assertTrue(endpoint.isPublished());
		assertEquals(SOAP_BINDING, endpoint.getBinding().getBindingID());

		// Data to access the web service
		final URL wsdlDocumentLocation = new URL(WS_ENDPOINT + "?wsdl");
		final String servicePart = "PruebaWsService";
		final String portName = "PruebaWsPort";
		final QName serviceQN = new QName(WS_NAMESPACE, servicePart);
		final QName portQN = new QName(WS_NAMESPACE, portName);

		// Creates a service instance
		final Service service = Service.create(wsdlDocumentLocation, serviceQN);
		final IPruebaWs ws = service.getPort(portQN, IPruebaWs.class);

		assertEquals("Hello word", ws.helloWord());

		// Unpublishes the SOAP Web Service
		endpoint.stop();
		assertFalse(endpoint.isPublished());
	}

}
