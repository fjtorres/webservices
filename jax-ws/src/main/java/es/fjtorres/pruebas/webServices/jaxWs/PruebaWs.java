package es.fjtorres.pruebas.webServices.jaxWs;

import javax.jws.WebMethod;
import javax.jws.WebService;

import es.fjtorres.pruebas.webServices.core.Coche;

@WebService
public class PruebaWs implements IPruebaWs {

	/* (non-Javadoc)
	 * @see es.fjtorres.pruebas.webServices.jaxWs.IPruebaWs#helloWord()
	 */
	@Override
	@WebMethod
	public String helloWord() {
		return "Hello word";
	}

	@Override
	@WebMethod
	public Coche getCoche() {
		Coche coche = new Coche();;
		coche.setAnio(2014);
		coche.setCocheId(1L);
		coche.setMarca("FORD");
		coche.setModelo("FOCUS");
		return coche;
	}

	@Override
	@WebMethod
	public void load(Coche coche){
		System.out.println(coche.getAnio());
	}
}

