package es.fjtorres.pruebas.webServices.jaxWs;

import javax.jws.WebService;

import es.fjtorres.pruebas.webServices.core.Coche;

@WebService
public interface IPruebaWs {

	String helloWord();
	Coche getCoche();
	void load(Coche coche);

}